
(function(root){


    package('player');
    intern('inventory', function(){

        var inventory = [];
        var maxSlots = 6;

        function add(item){
            if(inventory.length >= 6) return false;
            inventory.push(item);
            return true;
            
        }
        function take(item){
            var i = inventory.indexOf(item);
            if(i === -1) return false;
            var item = inventory[i];
            inventory.splice(i,1);
            return item;
        }
        

        return {
            add:add,
            take:take
        };
    });
    
    
    intern('hand',function(){
        
    });

    intern('health', 100);
    intern('willpower', 100);





}());