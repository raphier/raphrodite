package('player');
(function(bag){
    
   
    function putItem(node){
        
        var item = _token(node),
            isPossible,
            slot;
      

        function hasEmptySlot(){
            var slots = document.getElementById('bag');
            slots = slots.getElementsByTagName('td');
            for(var i=0,j=slots.length;i<j;i++){
                slot = slots[i];
                if(slot.textContent === 'empty') return true;
            }
            return false;
        }
        
        isPossible = (node.textContent !== 'empty' && hasEmptySlot()) ? true : false;
       
        if(isPossible && bag.add(item) ){
          
            node.textContent = 'empty';
            slot.textContent = item.name;
            slot['manud3290rtj0'+ document['dwedf23wrt34t']] = item;
           
            game.sandbox.text('You put '+ item.name +' to inventory.');
            _closeActionPanel(node.parentNode.lastChild);
            return;
        }
        game.sandbox.text('There is nothing in your hand.'); 


    }
    
    function takeItem(node){
        var item = _token(node),
            isPossible,
            handHandle = document.getElementById('hand');
       

        function emptyHand(){
            if(handHandle.textContent === 'empty')return true;
            game.sandbox.text('Your hands are full.'); 
            return false;
        }

        isPossible = (node.textContent !== 'empty' && emptyHand()) ? true : false ;

        if(isPossible){
            node.textContent = 'empty';
            item = bag.take(item);
            handHandle.textContent = item.name;
            handHandle['manud3290rtj0'+ document['dwedf23wrt34t']] = item;


            game.sandbox.text('You took '+ item.name +' from inventory.');
            
            launchActionPanel(handHandle.parentNode,0,handHandle);
            return;

        }
    }
    
    
    function insertItem(node){
        package('items');
        
        node['manud3290rtj0'+ document['dwedf23wrt34t']] = extern(node.textContent)();
        putItem(node);
        delete node['manud3290rtj0'+ document['dwedf23wrt34t']];
    }
    
    function remove(node){
        var parent = node.parentNode;
        parent.removeChild(node);
        delete parent;
        
    }
    
    
    function launchActionPanel(node,value,that){
        var item = _token(that);
        var l = $gin.html('<tr>').append(node);
        for(var method in item){
            var r = $gin.html('<tr>').append(l);
            if(typeof item[method] === 'string' || typeof item[method] === 'object') continue;
            $gin.html('<td>').attr('data-event','click').attr('data-action','itemAction').attr('data-value',method).text(method).append(r);
        }
    }
    
    
    function itemAction(node,value){
        var handHandle = document.getElementById('hand');
        var item = _token(handHandle);
        return item[value.value](handHandle);
        
    }
    
    
    function _closeActionPanel(node){
        remove(node);
    }
    
    function _token(node){
        return node['manud3290rtj0'+ document['dwedf23wrt34t']];
    }

    game.actions['putItem'] = putItem;
    game.actions['takeItem'] = takeItem;
    game.actions['insertItem'] = insertItem;
    game.actions['launchActionPanel'] = launchActionPanel;
    game.actions['remove'] = remove;
    game.actions['itemAction'] = itemAction;
    
    
}(extern('inventory')()));


// slot['manud3290rtj0'+ document['dwedf23wrt34t']] = item;

