
(function(){
    
    function destroyDiv(node,value){
        if(value === 0){
            var parent = node.parentNode.parentNode;
            parent.removeChild(node.parentNode);
        }
    }
    
    
    
    function destroyParent(node){
        var parent = node.parentNode.parentNode;
        parent.parentNode.removeChild(parent);
    }
    
    
    
    function countDown(node,value,that){
        var value = parseInt(that.getAttribute('data-value'));
        that.setAttribute('data-value',--value);
        
    }
    
    function linkToHouse(node,value,that){
        var value = parseInt(that.getAttribute('data-value'));
        if(value === 0){
            $gin.html('<a>').scribe('Go to house.').attr('data-event','click').attr('data-action','import')
            .attr('data-value','components/maps/house.htm')
            .append(that);
            
        }
    }
    
    function doImport(node,value){
        destroyDiv(node,0);
        var wrapper = document.getElementById('wrapper');
        $gin.html('<import>').attr('href',value.value || value).append(wrapper); 
    }
    
    
    function hide(node){
       
    }
    
    function unhide(){
        
    }

    
    
    
   game.actions['destroyDiv'] = destroyDiv;
   game.actions['destroyParent'] = destroyParent;
   game.actions['hide'] = hide;
   game.actions['unhide'] = unhide;
   game.actions['countDown'] = countDown;
   game.actions['linkToHouse'] = linkToHouse;
   game.actions['import'] = doImport;
    
}());




