
// Dependencies
var needle = require('needle');
var async = require('async');


var Connector = function(token,api){
    this.token = token;
    this.api = api || 'https://api.hipchat.com/v2/';
}

Connector.prototype = {
    
    notify: function(room, options, callback){
        this.request('post','room/'+room+'/notification', options, callback);
    },
    webhook: function(room, options, callback){
       this.request('post', 'room/'+room+'/webhook', options, callback);
    },
    webhooks: function(room, callback){
        this.request('get', 'room/'+room+'/webhook', callback);
    },
    delete_webhook: function(room,id, callback){
        needle.delete(this.url('room/'+room+'/webhook/'+id), null, callback );
    },
    request: function(type, path, payload, callback){
        if (this.isFunction(payload)) { // No payload
            callback = payload;
            payload = {};
        }
        var requestCallback = function (error, response, body) {
            //console.log('RESPONSE: ', error, response, body);
            // Connection error
            if (!!error) callback(new Error('HipChat API Error.'));
            // HipChat returned an error or no HTTP Success status code
            else if (body.hasOwnProperty('error') || response.statusCode < 200 || response.statusCode >= 300){
                try { callback(new Error(body.error.message)); }
                catch (e) {callback(new Error(body)); }
            }
            // Everything worked
            else {
                callback(null, body, response.statusCode);
            }
        };
        // GET request
        if (type.toLowerCase() === 'get') {
            var url = payload.hasOwnProperty('token') ? this.url(path, payload, payload.token) : this.url(path, payload);
            needle.get(url, requestCallback);
            // POST request
        } else if (type.toLowerCase() === 'post') {
            var url = payload.hasOwnProperty('token') ? this.url(path, payload.token) : this.url(path);
            needle.post(url, payload, {json: true, headers:{'Content-Type': 'application/json; charset=utf-8'}}, requestCallback);
            // PUT request
        } else if (type.toLowerCase() === 'put') {
            needle.put(this.url(path), payload, {json: true, headers:{'Content-Type': 'application/json; charset=utf-8'}}, requestCallback);
            // DELETE request
        } else if (type.toLowerCase() === 'delete') {
            needle.delete(this.url(path), {}, requestCallback);
            // otherwise, something went wrong
        } else {
            callback(new Error('Invalid use of the request function.'));
        }
    },
    // Generator API url
    url: function(rest_path, query, alternate_token){
        // inner helpers
        var BASE_URL = this.api + escape(rest_path) + '?auth_token=';
        var queryString = function(query) {
            var query_string = '';
            for (var key in query) {
                query_string += ('&' + key + '=' + query[key]);
            }
            return query_string;
        };
        if (arguments.length === 1) { // only contains path
            var url = BASE_URL + this.token;
            console.log('URL REQUEST: ', url);
            return url;
        } else if (arguments.length === 2) { // contains query or alt token
            if (typeof query === 'object') { // query {}
                var url = BASE_URL + this.token + queryString(query);
               console.log('URL REQUEST: ', url);
                return url;
            } else { // alt token
                alternate_token = query;
                var token = (alternate_token === undefined) ? this.token : alternate_token;
                var url = BASE_URL + token;
               console.log('URL REQUEST: ', url);
                return url;
            }
        } else if (arguments.length === 3) {
            var token = (alternate_token === undefined)? this.token : alternate_token;
            var url = BASE_URL + token + queryString(query);
            console.log('URL REQUEST: ', url);
            return url;
        }
    },
    isFunction: function(obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    }
   
};


module.exports = Connector;


function erback(callback,note){
    console.log(callback,note);
}


var botty = new Connector('EFiVJYmR0YAU8jmwqEL35czAGDqPGssmnTzOwnTS','https://thankgodnotmicrosoft.hipchat.com/v2/');

botty.notify('insaneroom',{message: 'baking stuff!',color:'green', token: 'aDUbmjQmASNx7soNef7W26h0I6hcymjkj3L39TtO'},erback);

//botty.webhook('Valet',{name:'83OUqdVKPfKMj5Mv8pHCYpRnhuCwmrsF8hk92KXr',event:'room_message',pattern: '(botty)[ \t]*([^\n\r]*)$',url:'http://www.liexport.net/test.php?name=213456'},erback);

//botty.delete_webhook('insaneroom',460054,erback);

//botty.delete_webhook('Valet',478293,erback);

botty.webhooks('Valet',erback);


